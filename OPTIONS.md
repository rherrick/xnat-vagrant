# XNAT Vagrant Project Configuration Options

Not all of the options below are relevant to every configuration. For example, options related to cloning and building the XNAT source–such as `branch` and `java_path`–are not referenced by the [xnat-release](configs/xnat-release/README.md) configuration.

Many of these options derive their default values from the value of other options. For example the default value for **xnat_home** is the value of **data_root** with **/home** appended to it. In cases like this, references to other options are _italicized_, so the default value for **xnat_home** would be _data_root_**/home**.

Some options are only used internally and shouldn't be specified or overridden. These are described in the [Internal Options section](#markdown-header-internal-options).

Option | Description | Default value(s)
------------- | ----------- | ------------------
admin | Specifies the email address to be set as the primary administrator contact for the XNAT server. | admin@miskatonic.edu
box | Specifies the Vagrant base box to use for the virtual machine. | Depends on configuration: [nrgxnat/xnatbox-xnat165-vivid-docker](https://app.vagrantup.com/nrgxnat/boxes/xnatbox-xnat165-vivid-docker) for [xnat-165](configs/xnat-165/README.md) and [nrgxnat/xnatstack-ubuntu2004](https://app.vagrantup.com/nrgxnat/boxes/xnatstack-ubuntu2004) for all others
branch | Specifies the default branch to use when cloning git repositories. | master
build | Indicates the script to be used when building the XNAT source repository. The path to the build script must be specified relative to the configuration folder. | [../../scripts/gradle-build.sh](scripts/gradle-build.sh)
context | Specifies the application context path for the deployed XNAT application. This is the part of the URL after the server address, e.g. https://server/path. To deploy without a context path, i.e. as the root application e.g. https://server, you can specify **ROOT** or just leave this unspecified. | ROOT
cpus | The number of CPUs dedicated to running the virtual machine. This is passed directly through to the [VirtualBox Vagrant provider configuration](https://www.vagrantup.com/docs/providers/virtualbox/configuration). | 1
data_root | Specifies the root folder to use for storing XNAT's data, including image and resource storage, configuration and home folders, and so forth. | /data/_project_
gui | Indicates whether the virtual machine should be launched in headless mode or with a GUI ("GUI" is a very broad term here: for the standard XNAT base boxes, the "GUI" is just a text-based terminal). This is passed directly through to the [VirtualBox Vagrant provider configuration](https://www.vagrantup.com/docs/providers/virtualbox/configuration). | false
home_pkg | Indicates the name of an archive file (zip, tar, tar.gz, or tar.bz2) located in the configuration folder. If the archive file exists, the contents of the archive are extracted into the XNAT user's home folder. |
host | The hostname to set for the virtual machine. This should be a simple name like **foo**, not a fully-qualified domain name which should be specified with the **server** option. | _name_
java_path | Specifies the path to the JDK installed on the virtual machine. | /usr/lib/jvm/java-8-openjdk-amd64
name | The name of the virtual machine. VirtualBox uses this for the virtual machine inventory. | Configuration name
pipeline_branch | Specifies the default branch to use when cloning the XNAT legacy pipeline engine's git repository. Note that installing the legacy pipeline engine is optional for XNAT 1.8._x_. This option will have no effect unless the **pipeline_src** option is specified. | _branch_
pipeline_inst | Specifies the default folder where the XNAT legacy pipeline engine should be installed. Note that installing the legacy pipeline engine is optional for XNAT 1.8._x_. This option will have no effect unless the **pipeline_src** option is specified. | _data_root_/pipeline
project | Indicates the XNAT project ID. | xnat
protocol | Indicates the protocol to use for the site URL. If you set **protocol** to **https** and don't specify a site URL or specify an **https** site URL, the provisioning script generates a self-signed certificate and configures your XNAT server to use **https**. | http
provision | Indicates the script to be used when provisioning the virtual machine. The path to the provisioning script must be specified relative to the configuration folder. | [../../scripts/provision.sh](scripts/provision.sh)
public_key | Specifies a path on the host VM to a public key. The contents of that public key are copied into both the XNAT and Vagrant users' authorized key files (~/.ssh/authorized__key). This enables ssh connections to the VM through public/private key authentication (you can find more information on public/private key [here](https://www.ssh.com/ssh/public-key-authentication)). |
ram | The amount of RAM in MB to allocate for the VM. This is passed directly through to the [VirtualBox Vagrant provider configuration](https://www.vagrantup.com/docs/providers/virtualbox/configuration). | 4096
server | The server address for the virtual machine. This can be the simple server name, a fully-qualified domain name like **xnatdev.xnat.org**, or just an IP address. | _vm_ip_
shares | Configures folders to be shared between the host and virtual machine. The format for this configuration is described in more detail in the [Advanced XNAT configuration section of the primary README.md](README.md#markdown-header-advanced-xnat-configuration). |
site | Used as the site ID for the newly deployed application. | XNAT
site_url | The URL to configure for the newly deployed application. | _protocol_://_server_/_context_
smtp | The address of the SMTP server to use for sending emails. | localhost
smtp_auth | Indicates whether XNAT should attempt to authenticate when connecting to the SMTP server. | false
smtp_password | The password to use when authenticating with the SMTP server. |
smtp_port | The port to use when connecting to the SMTP server. | 25
smtp_protocol | The protocol to use when connecting to the SMTP server. | smtp
smtp_username | The username to use when authenticating with the SMTP server. |
ssl_cert | Specifies the path for an SSL certificate to install in your VM. The path must be reachable from _within_ the VM. This usually means that it's located under the XNAT Vagrant repository and reachable via `/vagrant-root` or in the configuration folder and reachable via `/vagrant`. |
ssl_key | Specifies the path for the private key for the certificate indicated by the **ssl_cert** option. The same access rules apply for the specified location. |
ssl_subject | The subject to use when generating a self-signed SSL certificate. This is only used when the **protocol** option is set to **https**. | "/C=US/ST=Missouri/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=xnatdev.xnat.org"
update_box | Indicates whether the XNAT Vagrant base box should be updated before creating and provisioning the VM. | true
update_vbguest | Indicates whether the VirtualBox Guest Additions on the virtual machine should be updated during provisioning. This requires the [vagrant-vbguest plugin](https://github.com/dotless-de/vagrant-vbguest) be installed. If the plugin isn't installed, this option has no effect. | true
update_vm |  Indicates whether the operating system and installed services and dependencies should be updated while provisioning the VM. This is set to **false** by default because the update might take a while. | false
verbose | Causes more robust logging during provisioning. | false
vm_ip | The IP address to set for the VM. | Depends on configuration
xnat | Used to name source folders and mappings in configurations clone and/or build the XNAT source. | xnat
xnat_branch | Specifies the branch to use when cloning the XNAT git repository. | _branch_
xnat_home | The path to use for the XNAT user's home folder. | _data_root_/home
xnat_pass | The password to set for the VM user account. If you don't set a value for this setting, the password will not be set at all, meaning you can only access the VM through SSH public/private key pair authentication. Setting any value results in setting the password to that value. |
xnat_src | Specifies the path to a folder on the host machine with the XNAT source code. The path must be either absolute or specified relative to the configuration folder. |
xnat_user | The name of the user account to create on the VM. | _project_
xnat_version | Specifies the XNAT version to use. A value is only required for configurations that download the XNAT war. |

## Internal Options

Option | Description | Default value(s)
------------- | ----------- | ------------------
config | Indicates the name of the configuration being built. | Depends on configuration
deploy | Indicates how XNAT is deployed in the configuration. Values include **gradle-host**, **gradle-vm**, **release**, and **war**. | Depends on configuration
