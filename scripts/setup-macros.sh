#!/usr/bin/env bash

askToDeleteWork() {
    local ANSWER=""
    while true; do
        echo
        read -n 1 -s -r -p "Press Y to delete the cache folder, N to keep it, or X to cancel ) " ANSWER
        echo
        [[ ${ANSWER} =~ ^[NnXxYy]$ ]] && {
            RESPONSE=${ANSWER}
            break
        } || {
            echo
            echo "Please enter yes (Y), no (N), or exit (X)."
        }
    done
    echo
}

# quietly cd
cdq() {
    cd ${1} > /dev/null
}

checkWorkFolder() {
    RESPONSE=""

    [[ -d .work && $(find .work -type f ! -name status ! -name startup | wc -l | tr -d ' ') -gt 0 ]] && {
        workExists
    }

    [[ ! -d .work ]] && {
        mkdir -p .work
    }

    unset RESPONSE
}

countLines() {
    [[ -f ${1} ]] && { cat ${1} | wc -l | tr -d ' '; } || { echo 0; }
}

createVarsFiles() {
    # hack to force creation of vars.yaml by halting a non-existent VM
    [[ ! -f ./.work/vars.sh ]] && vagrant halt setup &>/dev/null

    # load existing or newly-created vars.sh
    [[ -f ./.work/vars.sh ]] && source ./.work/vars.sh
}

# Exit with error status
die() {
    echo "Exiting..."
    echo
    echo >&2 "${@}"
    echo
    exit -1
}

displayBanner() {
    echo
    echo "============================================================"
    echo "XNAT Vagrant"
    echo "============================================================"
    echo
}

getConfigsAsOptions() {
	for KEY in "${!CONFIGS[@]}"; do
  		echo "${KEY}"
	done | sort
}

getRunningVMs() {
    getVMs
    [[ -f .work/vbox-running-vms ]] && { cat .work/vbox-running-vm; }
}

getVMs() {
    # Clear existing state
    rm -f .work/*

    for VBOX in $(manageBoxes "list vms" | sed -E 's/^"([^"]+)".*/\1/'); do
        local VBOX_INFO=".work/vm_info_${VBOX}"
        manageBoxes "showvminfo ${VBOX}" > ${VBOX_INFO}
        VAGRANT_ROOT_FOLDER="$(cat ${VBOX_INFO} | grep 'vagrant-root' | sed -E 's/^.*Host path: '"'"'(.*)'"'"' .*$/\1/')"
        [[ "${VAGRANT_ROOT_FOLDER}" == "${VAGRANT_ROOT}" ]] && {
            local VM_STATE="$(cat ${VBOX_INFO} | grep -E '^State: ' | sed -E 's/^State:[[:space:]]+(.*)[[:space:]]+\(since.*$/\1/; s/ /-/g;')"
            echo "${VBOX}" >> .work/vbox-${VM_STATE}-vms
            echo "${VM_STATE}" > .work/vm_state_${VBOX}
        }
        rm -f ${VBOX_INFO}
    done
}

listDirs() {
    set -- ${1}*/
    printf "%s\n" "${@%/}"
}

manageBoxes() {
    bash -c "${VBOX_MANAGE_APP} ${@}"
}

prompt() {
    local REPROMPT="Please enter yes (Y) or no (N)"

    [[ ${1} == "withExit" ]] && {
        local WITH_EXIT="true"
        REPROMPT="Please enter yes (Y), no (N), or exit (X)."
        shift
    }

    local MESSAGE="${*}"

    local ANSWER=""
    while true; do
        echo
        read -n 1 -s -r -p "${MESSAGE} ) " ANSWER
        echo
        [[ ${ANSWER} =~ ^[Yy]$ ]] && { return 1; }
        [[ ${ANSWER} =~ ^[Nn]$ ]] && { return 0; }
        [[ ${ANSWER} =~ ^[Xx]$ && ! -z ${WITH_EXIT} ]] && { return 2; }
        echo
        echo "${REPROMPT}"
    done
    echo
}

promptWithExit() {
    prompt withExit ${*}
}

# Takes an array and presents it as a menu of options.
selectOption() {
    local  __result=${1}
    local SELECTION=''

    # Present a menu for them to choose from.
    while [[ -z ${SELECTION} ]]; do
        local INDEX=1
        for OPTION in "${OPTIONS[@]}"; do
            echo ${INDEX} - ${OPTION%%/}
            INDEX=$(expr ${INDEX} + 1)
        done
        echo
        read -p "Select: " SELECT
        echo
        if [[ -z ${SELECT} ]]; then
            echo "Invalid selection. Please choose one of the following:"
            echo
        else
            CHOICE=$(expr ${SELECT} - 1)
            if [[ ${CHOICE} -lt 0 || ${CHOICE} -ge ${#OPTIONS[@]} ]]; then
                echo "Invalid selection. Please choose one of the following:"
                echo
            else
                SELECTION=${OPTIONS[${CHOICE}]}
            fi
        fi
    done
    eval ${__result}="'${SELECTION}'"
}

showVMs() {
    getVMs

    echo
    RUNNING_COUNT=$(countLines .work/vbox-running-vms)
    SAVED_COUNT=$(countLines .work/vbox-saved-vms)
    POWERED_OFF_COUNT=$(countLines .work/vbox-powered-off-vms)
    [[ ${RUNNING_COUNT} == 0 && SAVED_COUNT == 0 && POWERED_OFF_COUNT == 0 ]] && {
        echo "------------------------------------------------------------"
        echo "There are currently no XNAT Vagrant VMs running, saved, or powered off."
        echo "------------------------------------------------------------"
    } || {
        [[ ${RUNNING_COUNT} -gt 0 ]] && {
            echo "Running XNAT Vagrant VMs:"
            echo "------------------------------------------------------------"
            echo "$(for RUNNING_VM in $(cat .work/vbox-running-vms); do echo " * ${RUNNING_VM}"; done;)"
            echo "------------------------------------------------------------"
            [[ ${SAVED_COUNT} -gt 0 || ${POWERED_OFF_COUNT} -gt 0 ]] && { echo; }
        }
        [[ ${SAVED_COUNT} -gt 0 ]] && {
            echo "Saved XNAT Vagrant VMs:"
            echo "------------------------------------------------------------"
            echo "$(for SAVED_VM in $(cat .work/vbox-saved-vms); do echo " * ${SAVED_VM}"; done;)"
            echo "------------------------------------------------------------"
            [[ ${POWERED_OFF_COUNT} -gt 0 ]] && { echo; }
        }
        [[ ${POWERED_OFF_COUNT} -gt 0 ]] && {
            echo "Powered off XNAT Vagrant VMs:"
            echo "------------------------------------------------------------"
            echo "$(for POWERED_OFF_VM in $(cat .work/vbox-saved-vms); do echo " * ${POWERED_OFF_VM}"; done;)"
            echo "------------------------------------------------------------"
        }
    }
    echo
    [[ ${CFG} =~ (__|--show-last|--show) ]] && echo && exit 0;
}

stackBoxUpdate() {
    # update the stack box if specified
    if [[ -n ${UPDATE_BOX} && ${UPDATE_BOX} == true ]]; then
        echo
        echo Updating source box...
        echo
        vagrant box update
    fi
}

workExists() {
    echo "The folder .work, which is used to store variables and configurations for use in building and managing your VM,"
    echo "already exists. Do you want to delete this folder and build your VM strictly from the values in your configuration"
    echo "files or re-use the cached configuration information?"

    promptWithExit "Press Y to delete the cache folder, N to keep it, or X to cancel"
    RESPONSE=${?}

    case ${RESPONSE} in
        0)
            echo "Keeping the folder .work..."
            ;;
        1)
            echo "Deleting the folder .work..."
            rm -rf .work
            ;;
        2)
            echo "Exiting..."
            exit 0;
            ;;
    esac
}

