#!/usr/bin/env bash

SERVER=${1}
openssl genrsa -des3 -passout pass:${SERVER} -out rootCA-${SERVER}.key 2048
openssl req -x509 -passin pass:${SERVER} -new -nodes -key rootCA-${SERVER}.key -sha256 -days 1024 -out rootCA-${SERVER}.pem -subj "/C=US/ST=Missouri/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=${SERVER}"
