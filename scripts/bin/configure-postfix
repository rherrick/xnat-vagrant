#!/bin/bash

OWD="$(cd "$(dirname "${0}")" && pwd -P)"

source ${OWD}/macros
source ${OWD}/vars.sh

showHelp() {
    local CMD_NAME="$(basename ${0})"
    local EXIT_STATUS="${1}"
    echo "${CMD_NAME} usage"
    echo
    echo " $ ${CMD_NAME} --server <server> <options>"
    echo
    echo " Options:"
    echo
    echo "  * -h | --help       Display this help message"
    echo "  * -s | --server     The SMTP server address (required)"
    echo "  * -p | --port       The SMTP server port (defaults to 587 if username is specified, 25 if not)"
    echo "  * -H | --host       The mail host (defaults to ${HOST})"
    echo "  * -u | --username   The username for accessing the SMTP server"
    echo "  * -P | --password   The password for accessing the SMTP server"
    echo "  * -t | --test-to    Email address to specify as to address on test email"
    echo "  * -f | --test-from  Email address to specify as from address on test email"
    echo
    exit ${EXIT_STATUS}
}

while (( "${#}" )); do
    case "${1}" in
        -s|--server)
            SMTP_SERVER=${2}
            shift 2
            ;;
        -p|--port)
            SMTP_PORT=${2}
            shift 2
            ;;
        -u|--username)
            SMTP_USERNAME=${2}
            shift 2
            ;;
        -P|--password)
            SMTP_KEY=${2}
            shift 2
            ;;
        -f|--test-from)
            TEST_FROM=${2}
            shift 2
            ;;
        -H|--host)
            HOST=${2}
            shift 2
            ;;
        -t|--test-to)
            TEST_TO=${2}
            shift 2
            ;;
        -h|--help)
            showHelp 0
            ;;
        -*|--*=)
            echo "Error: unknown option ${1}" >&2
            showHelp 1
            ;;
    esac
done

# Server address is required.
[[ -z ${SMTP_SERVER} ]] && { echo "You must specify a value for the SMTP server (-s or --server)."; echo; showHelp 255; }
# If no port is specified, default to 25 if no username is specified, 587 if username is specified.
[[ -z ${SMTP_PORT} ]] && { [[ -z ${SMTP_USERNAME} ]] && { SMTP_PORT=25; } || { SMTP_PORT=587; }; }
# If username is specified and no password is specified, prompt user for password.
[[ ! -z ${SMTP_USERNAME} && -z ${SMTP_KEY} ]] && { IFS= read -rsp "Please enter the password or token for the SMTP server: " SMTP_KEY < /dev/tty; echo; }

echo "postfix postfix/mailname string ${HOST}" | sudo debconf-set-selections
echo "postfix postfix/main_mailer_type string 'Internet Site'" | sudo debconf-set-selections
sudo ${INSTALL_CMD} install -y libsasl2-modules postfix
sudo cp /etc/postfix/main.cf /etc/postfix/main.cf.orig
sudo sed --in-place "s/^relayhost.*$/relayhost = [${SMTP_SERVER}]:${SMTP_PORT}/" /etc/postfix/main.cf
LEVEL_COUNT=$(grep -E 'smtp_tls_security_level[ =]' /etc/postfix/main.cf | wc -l | tr -d ' ')
[[ ${LEVEL_COUNT} == 0 ]] && {
    echo | sudo tee --append /etc/postfix/main.cf > /dev/null
    echo "# Enable STARTTLS encryption" | sudo tee --append /etc/postfix/main.cf > /dev/null
    echo "smtp_tls_security_level = encrypt" | sudo tee --append /etc/postfix/main.cf > /dev/null
    echo | sudo tee --append /etc/postfix/main.cf > /dev/null
} || {
    sudo sed --regexp-extended --in-place "s/^smtp_tls_security_level[ =]+.*$/smtp_tls_security_level = encrypt/" /etc/postfix/main.cf
}

[[ ! -z ${SMTP_USERNAME} ]] && {
    cat /vagrant-root/templates/postfix.main.cf.tmpl | sudo tee --append /etc/postfix/main.cf > /dev/null
    sudo mkdir -p /etc/postfix/sasl
    cat /vagrant-root/templates/sasl_password.tmpl | sed 's/@SMTP_SERVER@/'"${SMTP_SERVER}"'/g' | sed 's/@SMTP_PORT@/'"${SMTP_PORT}"'/g' | sed 's/@SMTP_USERNAME@/'"${SMTP_USERNAME}"'/g' | sed 's/@SMTP_KEY@/'"${SMTP_KEY}"'/g' | sudo tee /etc/postfix/sasl/sasl_password > /dev/null
    sudo postmap /etc/postfix/sasl/sasl_password
    sudo chmod 600 /etc/postfix/sasl/*
}

sudo systemctl restart postfix

[[ "$(sudo find /var/spool/postfix/public -name pickup | wc -l)" == "0" ]] && { sudo mkfifo /var/spool/postfix/public/pickup; }

[[ -n ${TEST_FROM} && -n ${TEST_TO} ]] && {
    SED_FILE=$(getSedFile)
    SED_FROM="s/@TEST_FROM@/${TEST_FROM}/g"
    SED_TO="s/@TEST_TO@/${TEST_TO}/g"
    [[ $(fgrep ${SED_FROM} ${SED_FILE} | wc -l) == 0 ]] && { sudo sed -i '/s\/@TEST_FROM@.*/d' ${SED_FILE}; echo "${SED_FROM}" | sudo tee -a ${SED_FILE} > /dev/null; }
    [[ $(fgrep ${SED_TO} ${SED_FILE} | wc -l) == 0 ]] && { sudo sed -i '/s\/@TEST_TO@.*/d' ${SED_FILE}; echo "${SED_TO}" | sudo tee -a ${SED_FILE} > /dev/null; }
    echo Testing Postfix configuration, sending email from: "${TEST_FROM}" to: "${TEST_TO}"
    sendmail ${TEST_TO} <<<$(replaceTokens postfix.test.email)
}

