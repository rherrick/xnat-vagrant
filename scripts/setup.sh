#!/bin/bash

# wipe out .vagrant folder - it will be recreated
[[ -d .vagrant ]] && rm -R .vagrant

#echo
#echo Downloading XNAT war file...

mkdir -p ../../local/downloads
DL_DIR=../../local/downloads

getConfigValueFromFile() {
    echo $(grep ^$2 $1 | cut -f 2- -d : | sed -e 's/#.*$//' | sed -e 's/^[[:space:]]*//')
}

getConfigValue() {
    local VALUE=$(getConfigValueFromFile config.yaml $1)
    [[ -f local.yaml ]] && VALUE=$(getConfigValueFromFile local.yaml $1)
    echo ${VALUE}
}

doDownload() {
    [[ -e $1 ]] && rm $1
    echo
    echo Downloading from configured URL: $2
    cd ${DL_DIR}
    curl --location --fail --retry 5 --retry-delay 5 --remote-name $2
    local STATUS=${?}
    cd -
    [[ ${STATUS} -gt 0 ]] && { echo "Error downloading $1"; exit ${STATUS}; }
}

downloadPrompt(){
    local choice=x
    if [[ -e $1 ]]; then
        prompt "${2##*/} has already been downloaded. Type 'y' to download a new copy or 'n' to continue."
    else
        choice=1
    fi
    [[ $choice == 1 ]] && { doDownload $1 $2; }
}

XNAT_URL=$(getConfigValue xnat_url)
XNAT_WAR=${DL_DIR}/${XNAT_URL##*/}
downloadPrompt ${XNAT_WAR} ${XNAT_URL}

PIPELINE_URL=$(getConfigValue pipeline_url)
[[ ! -z ${PIPELINE_URL} ]] && {
    PIPELINE_ZIP=${DL_DIR}/${PIPELINE_URL##*/}
    [[ ${PIPELINE_ZIP} =~ ^\d.* ]] && { PIPELINE_ZIP="xnat-pipeline-${PIPELINE_ZIP}"; }
    downloadPrompt ${PIPELINE_ZIP} ${PIPELINE_URL}
}

echo
echo Starting XNAT build...

echo
echo Provisioning VM with specified user...

echo
vagrant up

echo
echo Reloading VM to configure folder sharing...

echo
vagrant reload

echo
echo Running build provision to build and deploy XNAT on the VM...

echo
vagrant provision --provision-with build

echo
echo Provisioning completed.
