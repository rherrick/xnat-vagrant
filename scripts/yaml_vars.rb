#!/usr/bin/env ruby
# convert YAML properties to vars for setup scripts

require 'yaml'
require 'fileutils'

load '../../scripts/ssl_config.rb'

# pass the config directory as the first argument when executing this script
# ./yaml_vars.rb ../configs/foo
cfg_name = File.basename(Dir.getwd)
cfg_dir ||= ARGV[0] || cfg_name
cwd ||= File.dirname(File.expand_path(cfg_name))

Dir.mkdir("#{cwd}/.work") unless File.exists?("#{cwd}/.work")

# load config settings
puts "Loading #{cwd}/config.yaml"
profile ||= YAML.load_file("#{cwd}/config.yaml")

# load common overrides from configs/global.yaml
global_config = "#{cwd}/../../configs/global.yaml"
if File.exists?(global_config)
    puts "Loading global overrides from #{global_config}..."
    global_yaml = YAML.load_file(global_config)
    global_yaml.each { |k, v|
        profile[k] = v
    }
end

# load local customizations
# (We really want to read these last, but we need to read the config file from here.)
local_path = "#{cwd}/local.yaml"
if File.exists?(local_path)
    puts "Loading local overrides from #{local_path}..."
    local = YAML.load_file(local_path)
    local.each { |k, v|
        profile[k] = v
    }
end

# set name to config folder name if not defined
profile['name'] ||= cfg_name

# setup some fallback defaults - some of these are for backwards compatibility
profile['xnat'] ||= 'xnat'
profile['verbose'] ||= false
profile['public_key'] ||= ''
profile['context'] ||= ''
profile['project'] ||= 'xnat'
profile['host'] ||= profile['name']
profile['data_root'] ||= "/data/#{profile['project']}"
profile['admin'] ||= "admin@#{profile['server']}"
profile['xnat_user'] ||= profile['project']
profile['xnat_pass'] ||= ''
profile['xnat_home'] ||= "#{profile['data_root']}/home"
profile['home_pkg'] ||= ''
profile['server'] ||= profile['vm_ip']
profile['xnat_version'] ||= ''
profile['fix_context'] ||= false
profile['pipeline_version'] ||= profile['xnat_version']
profile['branch'] ||= 'master'
profile['xnat_branch'] ||= profile['branch']
profile['pipeline_branch'] ||= profile['branch']
profile['pipeline_inst'] ||= 'pipeline'

profile['config'] ||= (cfg_dir || '')
profile['provision'] ||= ''
profile['build'] ||= ''

if profile['server'] == ''
    profile['server'] = profile['vm_ip']
end

if profile['context'] == ''
    profile['context'] = 'ROOT'
end

# Memory settings: total VM RAM, minimum heap for XNAT JVM, maximum heap for XNAT JVM
profile['ram'] ||= '4096'
profile['min_heap'] ||= '512m'
profile['max_heap'] ||= '2g'

# build siteUrl setting from protocol and server
profile['protocol'] ||= 'http'
profile['site'] ||= 'XNAT'
profile['site_url'] ||= "#{profile['protocol']}://#{profile['server']}#{profile['context'] == 'ROOT' ? '' : '/' + profile['context']}"

if profile['xnat_url'] || profile['xnat_repo']
    profile['xnat_src'] = profile['xnat_url'] ||= profile['xnat_repo']
end

if profile['pipeline_url'] || profile['pipeline_repo']
    profile['pipeline_src'] = profile['pipeline_url'] ||= profile['pipeline_repo']
end

ssl_config = SslConfig.new profile
ssl_config.process cwd

shares = profile['shares'] ||= ''
has_shares = shares && shares != 'false' && shares != ''

# If there's a setting for public key...
unless profile['public_key'].to_s.strip.empty?
    # Try to find that file
    if File.exists?(profile['public_key'])
        # If it exists, get it and just make sure it's not empty.
        keys = IO.readlines(profile['public_key'])
        if keys.length > 0
            puts "Retrieved public key from file #{profile['public_key']}"
            profile['public_key'] = keys[0].delete!("\n")
        else
            puts "Found the specified public key file at #{profile['public_key']}, but it appears to be empty."
            profile['public_key'] = ''
        end
    else
        puts "Public key file specified as \"#{profile['public_key']}\", but I can't find that."
        profile['public_key'] = ''
    end
end

# tolerate old settings for upgrading the VM OS and packages
profile['update_vm'] ||= false

# tolerate old settings for updating VM guest additions
profile['update_vbguest'] ||= false

# write to '.work/vars.yaml' file to serve as a Single Point of Truth after setup
puts "Initializing #{cwd}/.work/vars.yaml"
File.open("#{cwd}/.work/vars.yaml", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("# vars.yaml\n")
    vars.puts("# DO NOT EDIT THE CONTENTS OF THIS FILE.\n")
    vars.puts("# IT IS AUTOMATICALLY GENERATED ON SETUP.\n")
    # leverage the .to_yaml method
    #noinspection RubyMismatchedReturnType
    vars.puts profile.to_yaml
}

puts "Initializing #{cwd}/.work/vars.sh"
File.open("#{cwd}/.work/vars.sh", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("#!/usr/bin/env bash\n")
    profile.each { |k, v|
        if has_shares && %w[shares].include?(k)
            if profile['verbose']
                puts " * #{k.upcase}='#{v}'"
            end
            vars.puts "#{k.upcase}='#{v}'"
        else
            # empty values get changed to ""
            if profile['verbose']
                puts " * #{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
            end
            vars.puts "#{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
        end
    }
}

puts "Initializing #{cwd}/.work/vars.sed"
File.open("#{cwd}/.work/vars.sed", 'wb') { |vars|
    vars.truncate(0)
    profile.each { |k, v|
        # exclude shares setting
        unless %w[shares shared share].include?(k)
            _v = "#{v}"
            if profile['verbose']
                puts " * s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
            end
            vars.puts "s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
        end
    }
}
