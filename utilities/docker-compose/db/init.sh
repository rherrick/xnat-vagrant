#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username="${POSTGRES_USER}" --dbname=template1 <<-EOSQL
-- Install trigram extension
CREATE EXTENSION pg_trgm;

-- Configure XNAT database
CREATE USER xnat CREATEDB;
ALTER USER xnat WITH PASSWORD 'xnat';
CREATE DATABASE xnat OWNER xnat;

-- Configure Orthanc database
CREATE USER orthanc NOCREATEDB;
ALTER USER orthanc WITH PASSWORD 'orthanc';
CREATE DATABASE orthanc OWNER orthanc;
EOSQL
