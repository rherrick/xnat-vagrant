# Docker Compose for XNAT External Services #

This [docker-compose](https://docs.docker.com/compose) project can be used to configure and control useful
services for your XNAT Vagrant VM. Providing these services outside of your XNAT VM allows greater flexibility
when developiing and testing XNAT against different service versions, configurations, etc.

Currently this project supports the following features:

* [LDAP repository](features/ldap/README.md). This creates an LDAP repository populated with user accounts that 
  can be used to test LDAP integration with XNAT authentication.
* [ActiveMQ service](features/mq/README.md). This creates an ActiveMQ server that can be used to test coordination
  and task sharing between multiple XNAT nodes.
* [PACS](features/pacs/README.md). This creates an Orthanc PACS system that can be used to test DICOM sends to
  XNAT as well as query-and-retrieve functionality when the [DQR plugin](https://bitbucket.org/xnatdev/dicom-query-retrieve)
  is installed on your XNAT VM.
* [SMTP server](features/smtp/README.md). This creates a Postfix relay service that can be configured as the SMTP
  server for your XNAT VM. Handling the SMTP configuration in the service configuration simplifies the XNAT mail
  configuration.

Each feature has a default configuration, but may require custom configuration to replicate particular scenarios.
Specific detail on configuring each feature is provided in the **README.md** file in each feature folder.

## Requirements ##

The only requirement for using this **docker-compose** project is that you have Docker installed. All current Docker
installations include support for **docker-compose**.

## Starting ##

This project provides a wrapper script for controlling your services. To invoke it, simply cd to this directory and
run the script:

```
$ ./xnat-svcs
```

Without any arguments, the script displays its built-in help menu:

```
Usage: ././xnat-svcs [feature1|feature2] [file1.env file2.env]
       ././xnat-svcs all [file1.env file2.env]

Features include:

 * ldap
 * smtp
 * mq
 * pacs

Specifying "all" includes all available features. Don't mix "all"
and individual features on the command line.

Any file ending in ".env" is presumed to be an environment file. If
you don't specify any ".env" files, this script looks for files in the
current directory with that extension and automatically adds them as
environment files.
```

The script supports a number of options:

* **help** displays a short help message and terminates
* **all** launches with all available features enabled
* **_feature_** launches with a particular feature
* Any argument that ends in **.env** is added as an environment file

In addition, you can specify **start** and **stop** commands. If neither is specified, **start** is presumed.

If you don't specify any features or the **all** option, the script launches a minimal set of services, which
includes:

* PostgreSQL
* PGAdmin4

## Stopping ##

To stop your services, run the command:

```
$ ./xnat-svcs stop
```

The **stop** command uses the contents of a cache file named **.config** to shut down your services. You should
not delete this file if found!

## Accessing Services ##

You can access these external services both from your desktop and a running XNAT Vagrant VM.

### Desktop Access ###

Services running in Docker containers can be accessed through **localhost**, distinguishing between particular
services by the ports mapped in the **docker-compose.yml** configuration for each service:

* PostgreSQL connections (e.g. with the **psql** command-line client or database browser tools like **DataGrip**)
  can be made through port 5432
* PGAdmin4 can be accessed at http://localhost:8888. The default credentials are **info@xnat.org**, password **postgres**.
* LDAP can be 
* ActiveMQ can be accessed via the JMS default port 61616. The admin UI can be accessed at http://localhost:8161.
  The default administrator credentials are **admin** and **password**. The default write credentials are **write** and
  **password**.


The services running under **docker-compose** will not be on the same network as your XNAT Vagrant VM, so network

