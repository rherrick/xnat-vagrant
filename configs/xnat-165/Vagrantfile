# -*- mode: ruby -*-
# vi: set ft=ruby :

#
# XNAT Vagrant project
# http://www.xnat.org
# Copyright (c) 2015-2016, Washington University School of Medicine, all rights reserved.
# Released under the Simplified BSD license.
#

require 'yaml'

# save the root 'multi' dir
multi_root = '../..'

cwd     = File.dirname(File.expand_path(__FILE__))
cfg_dir = File.basename(Dir.getwd)

Dir.mkdir("#{cwd}/.work") unless File.exists?("#{cwd}/.work")

# load config settings
puts "Loading #{cwd}/config.yaml for Vagrant configuration..."
profile    = YAML.load_file("#{cwd}/config.yaml")

# load local customizations
# (We really want to read these last, but we need to read the config file from here.)
local_path = "#{cwd}/local.yaml"
local      = {}
if File.exists? (local_path)
    puts "Loading local overrides from #{local_path}..."
    local = YAML.load_file(local_path)
    local.each { |k, v|
        profile[k] = v
    }
end

profile['host'] ||= profile['name']
profile['box'] ||= 'nrgxnat/xnatbox-xnat165-vivid-docker'

File.open("#{cwd}/.work/vars.sh", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("#!/bin/bash\n")
    profile.each { |k, v|
        vars.puts "#{k.upcase}='#{v}'"
    }
}

File.open("#{cwd}/.work/vars.sed", 'wb') { |vars|
    vars.truncate(0)
    profile.each { |k, v|
        # Only put v in the sed file if it's a string. No subs for hashes.
        if v.is_a?(String)
            vars.puts "s/@#{k.upcase}@/#{v.gsub('/', "\\/")}/g"
        end
    }
}

shares     = profile['shares'] ||= profile['shared'] ||= profile['share']
has_shares = shares && shares != 'false'

API_VERSION = '2'

Vagrant.configure(API_VERSION) do |config|

    config.vm.define "#{profile['name']}"
    config.vm.box     = profile['box']
    config.vm.network 'private_network', ip: profile['vm_ip']
    config.vm.hostname = profile['host']

    config.vm.provider 'virtualbox' do |v|
        v.name   = profile['name']
        v.memory = profile['ram']
        v.cpus   = profile['cpus']
        v.gui    = profile['gui']
    end

    if Vagrant.has_plugin?("vagrant-vbguest")
      config.vbguest.auto_update = profile['update_vbguest'] ||= false
    end

    # set the main folder as a share
    config.vm.synced_folder "#{multi_root}", '/vagrant-root'

    if has_shares
        shares.each { |share, share_to|
            puts "Setting up share from #{share} to #{share_to[0]}"
            config.vm.synced_folder share, share_to[0], mount_options: share_to[1]
        }
    end

end
