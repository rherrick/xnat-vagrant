#!/usr/bin/env bash

source ../../scripts/setup-macros.sh

# remove .vagrant and .work folders on setup
#rm -Rf ./.vagrant ./.work

# ...it's actually nicer to ask first:
checkWorkFolder
createVarsFiles
stackBoxUpdate

echo
echo Starting XNAT build...

echo
echo Provisioning VM with specified user...

echo
vagrant up

echo
echo Reloading VM to configure folder sharing...

echo
vagrant reload

echo
echo Running build provision to build and deploy XNAT on the VM...

echo
vagrant provision --provision-with build

echo
echo Automated provisioning completed. To complete setup, a compiled
echo XNAT .war file will need to be deployed as 'ROOT.war' to the
echo Tomcat webapps folder '/var/lib/tomcat8/webapps'.
