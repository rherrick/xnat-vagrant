# XNAT Vagrant

This is the repository for the [XNAT](http://www.xnat.org) [Vagrant project](https://bitbucket.org/xnatdev/xnat-vagrant).

## Quick-Start

> If you are running on Windows, you need a bash terminal program. There are a number of different options available:
 - [Cygwin](https://www.cygwin.com)
 - Git Bash (part of the [Git for Windows](https://gitforwindows.org) application)
 - [Ubuntu on Windows Subsystem for Linux](https://ubuntu.com/wsl)

To use the XNAT Vagrant project:

- Make sure you have [Git](https://git-scm.com/downloads), [Vagrant](https://www.vagrantup.com), and [VirtualBox](https://www.virtualbox.org) installed on your host machine. Currently the XNAT Vagrant project only supports the VirtualBox Vagrant provider.
- Clone this repo: `git clone https://bitbucket.org/xnatdev/xnat-vagrant.git`
- From inside the `xnat-vagrant` folder, run `./run xnat-release setup` to launch and configure a Vagrant VM using the [latest pre-built XNAT release war](https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-1.8.4.war). Other VM configurations can be set up similarly, substituting the folder name of the config: `./run xnat-latest setup`, etc. You can see the available configurations by listing the contents of the [configs](configs) folder.

The XNAT Vagrant project provides the following configurations:

Configuration | Description | Server address | Default IP address
------------- | ----------- | -------------- | ------------------
[xnat-dev](configs/xnat-dev/README.md) | Mounts XNAT source (and optionally pipeline) folders into the VM to facilitate deploying XNAT development. | xnatdev.xnat.org | 192.168.56.100
[xnat-release](configs/xnat-release/README.md) | Downloads the latest XNAT release bundle and installs it in the VM. | release.xnat.org | 192.168.56.101
[xnat-latest](configs/xnat-latest/README.md) | Clones the XNAT web _source_ code from the [xnat-web git repository](https://bitbucket.org/xnatdev/xnat-web) to allow you to build the latest XNAT code _inside_ the VM itself. | latest.xnat.org | 192.168.56.102
[xnat-system](configs/xnat-system/README.md) | Sets up the virtual machine to run XNAT but does _not_ install the XNAT war file. | xnatsys.xnat.org | 192.168.56.99
[xnat-165](configs/xnat-165/README.md) | Builds a VM using the XNAT 1.6.5 box image, which provides the XNAT 1.6.5 release server. | xnat165.xnat.org | 192.168.56.165
[xnat-176](configs/xnat-176/README.md) | Builds a VM installing the XNAT 1.7.6 release server. | xnat176.xnat.org | 192.168.56.176

The DNS server for [xnat.org](https://www.xnat.org) maps the server addresses above to the corresponding default IP addresses. That means that, when you use the default IP address for a particular configuration, you can access that VM through the server address without having to perform any further configuration. For example, if you stand up the [xnat-release](configs/xnat-release/README.md) configuration, you can use [release.xnat.org](https://release.xnat.org) to access your XNAT without having to configuration that address in your system's `hosts` configuration.

## Changes from previous releases

### IP addresses

The IP addresses for XNAT Vagrant have changed from previous releases. This was required by a change to VirtualBox, which restricted the valid range for IP addresses in host-only networking to `192.68.56.0/21`. You can use online tools like this [IP Address In CIDR Range Check](https://tehnoblog.org/ip-tools/ip-address-in-cidr-range) to validate any custom IP addresses you might assign. The default IP addresses for the XNAT Vagrant configurations are all valid in this range, but older VM configurations may fail to start properly and any configuration changes (e.g. modifications to your host machine's `/etc/hosts` file) will need to be modified.

The changes to IP addresses for XNAT Vagrant's default configurations are described in the table below.

Configuration | Server address | Previous IP address | Default IP address
------------- | -------------- | ------------------- | ------------------
xnat-dev | xnatdev.xnat.org | 10.1.1.170 | 192.168.56.100
xnat-release | release.xnat.org | 10.1.1.17 | 192.168.56.101
xnat-latest | latest.xnat.org | 10.1.100.17 | 192.168.56.102
xnat-system | xnatsys.xnat.org | 10.100.100.17 | 192.168.56.99
xnat-165 | xnat165.xnat.org | 10.1.1.165 | 192.168.56.165
xnat-176 | xnat176.xnat.org | 10.1.1.176 | 192.168.56.176

As noted above, the server addresses are mapped in the DNS server for [xnat.org](https://www.xnat.org) to the new default IP addresses.

### Improved SSL certificate handling

Earlier releases included a script that generated self-signed SSL certificates during provisioning. This release has expanded this functionality:

* Creates a self-signed certificate authority that can be used to generate multiple self-signed SSL certificates
* Allows re-use of previously generated certificate authorities and SSL certificates
* Allows using "real" SSL certificates issued by a trusted certificate authority
* [Port forwarding](#markdown-header-port-forwarding) can be used to forward port 443 from the VM to the host, which–combined with a valid SSL certificate–provides an easy way to serve XNAT functionality externally without configuring the host machine

SSL certificate handling is described in more detail in [SSL.md](SSL.md).

## List of commands:

- `./run` _config_ `setup` - initial VM setup - this __must__ be performed first to create the VM
- `./run` _config_ `stop` - shuts down the VM
- `./run` _config_ `start` - (re)launches a VM that has been set up but is not running
- `./run` _config_ `destroy` - permanently deletes the VM and related files from the host

The `run` script is more or less a proxy for the `vagrant` commands, allowing you to work with multiple VMs from a single root folder. You can also navigate to each individual configuration folder and run the `setup.sh` scripts or the Vagrant commands directly.

```bash
$ cd configs/xnat-release
$ ./setup.sh
```

## Configuration

In each folder, you can set up a file named **local.yaml** to customize various attributes of your Vagrant VM. Each folder contains a version of **sample.local.yaml** that you can use as the starting point for your own **local.yaml** file. Reference the **config.yaml** file in that configuration to see the default values that are passed into the Vagrant configuration.

### VM options

Many of the standard options in the **local.yaml** file are fairly straightforward and primarily affect basic traits about how the VM is configured and XNAT is initialized:

Option | Description | Default value(s)
------ | ----------- | ----------------
name | The name of the VM. This is used as the VM name in the VirtualBox inventory. | Depends on configuration
host | The host name to set for the VM. | The value of **name**
server | The server address for the VM. | VM's IP address
project | The project ID. This is used for naming various items within XNAT. | **xnat**
site | The site ID. | **XNAT**
xnat_user | The name of the user account to create on the VM. | The value of **project**
xnat_pass | The password to set for the VM user account. If you don't set a value for this setting, the password will not be set at all, meaning you can only access the VM through SSH public/private key pair authentication. Setting any value results in setting the password to that value. |
public_key | Specifies a path on the host VM to a public key. The contents of that public key are copied into both the XNAT and Vagrant users' authorized key files (~/.ssh/authorized__key). This enables ssh connections to the VM through public/private key authentication (you can find more information on public/private key [here](https://www.ssh.com/ssh/public-key-authentication)). |
vm_ip | The IP address to set for the VM. | Depends on configuration
ram | The amount of RAM in MB to allocate for the VM. | 4096
cpus | The number of CPU cores to allocate for the VM. This defaults to 1. | 1

There are many other configurable options, which are described in more detail in [OPTIONS.md](OPTIONS.md).

### Advanced XNAT configuration

This section contains the following topics:

* [Port forwarding](#markdown-header-port-forwarding)
* [Sharing folders between host and guest](#markdown-header-sharing-folders-between-host-and-guest)
* [Initializing the database from an SQL dump file](#markdown-header-initializing-the-database-from-an-sql-dump-file)
* [Reusing XNAT data archive folders across VM setups](#markdown-header-reusing-xnat-data-archive-folders-across-vm-setups)
* [Configuring the VM user home folder](#markdown-header-configuring-the-vm-user-home-folder)
* [Configuring SMTP](#markdown-header-configuring-smtp)

#### Port forwarding



#### Sharing folders between host and guest

As noted, there are a few options you can set in **local.yaml** that affect how XNAT itself is configured in the VM. To achieve more advanced configuration, for example configuring email and notification options, using an external existing database, and even mounting XNAT to use an existing data repository, you can mount shared folders that contain XNAT configuration and initialization files or data repositories.

The standard folders in the XNAT user's home folder are:

- **config** contains the XNAT configuration and initialization file(s)
- **logs** contains the XNAT application logs
- **plugins** contains any installed XNAT plugin libraries
- **work** contains temporary files for managing downloads, server work, etc.

The **xnat-vagrant** **.gitignore** file contains entries for these folders at the root level of the project, meaning you can create and mount these folders without them showing up as changes to the source-controlled project. To mount these folders on your VM, you need to configure them in a **shares** section in your **local.yaml** file. The code below demonstrates how to mount the **config** and **plugins** folders.

```yaml
shares:
    '/path/to/config':
        - '/data/xnat/home/config'
        - ['fmode=644','dmode=755']
    '/path/to/plugins':
        - '/data/xnat/home/plugins'
        - ['fmode=644','dmode=755']
```

In this configuration, the first path, e.g. **/path/to/config**, indicates the location of a folder on your host or local machine. This means you can re-use a particular configuration for multiple VMs ((although this can cause issues if multiple XNATs are running against the same database).

#### Initializing the database from an SQL dump file

During the default provisioning of the XNAT Vagrant VM, a role and database are created for your XNAT server. The database role uses the same value as the user account, i.e. **xnat_user**, while the database is named with the value for the **project** setting. By default, the database itself is unpopulated until XNAT starts and creates its default data schemas. If you'd like to be able to start from a saved database state, you can specify the location of an SQL dump file with the **psql_import** option. Once the role and database are provisioned, the provisioning script checks for this value and, if set, imports it into the database.

> **Note:** The path indicated by the value specified for **pg_import** must be local to the virtual machine and not indicate a path on the host machine! You can put the SQL dump file into the folder from which you're building your Vagrant VM and reference it via something like **/vagrant/_dumpfile.sql_**. Alternatively, you could put the file in a folder on the host machine then mount that folder as a shared folder on the VM:

```yaml
psql_import: /data/xnat/support/xnat.sql
shares:
    '/path/to/files':
        - '/data/xnat/support'
        - ['fmode=644','dmode=755']
```

You can create an SQL dump file from an existing XNAT installation or VM using the standard PostgreSQL command **pg_dump**. The command below shows how you could dump the database to SQL using the same file path as shown above:

```bash
$ pg_dump xnat > /data/xnat/support/xnat.sql
```

#### Reusing XNAT data archive folders across VM setups

You can save the data created by your Vagrant VM across destroy and setup operations by storing the XNAT data archive folder on the host machine, then sharing that folder to the VM through a shared folder:

```yaml
shares:
    '/path/to/local/archive':
        - '/data/xnat/archive'
        - ['fmode=644','dmode=755']
```

Combined with dumping then restoring the database from an SQL dump file, this allows you to migrate server state across multiple iterations of your VM.

#### Configuring the VM user home folder

You can set up the VM user's home folder (e.g., **/data/xnat/home**) as a shared folder, but you must make sure you set it up properly.

> **Note:** An improperly configured home folder can cause lots of problems, including preventing you from accessing the VM through ssh. It's usually best to share the folders required to bootstrap your XNAT VM, such as **config**, **plugins**, and **archive**, so that you can re-use configuration and initialization files and existing data stores. Mounting the entire VM user folder allows you to configure the user's shell environment through custom **.bashrc** and **.bash_aliases**, provide custom tools through something like a local **bin** folder, and so on.

The configuration for using a shared home folder for your VM user should look something like this (this example uses the default VM user **xnat**):

```yaml
shares:
    '../../../home':
        - '/data/xnat/home'
        - ['fmode=644','dmode=755']
    '../../../home/.ssh':
        - '/data/xnat/home/.ssh'
        - ['fmode=600','dmode=700']
```

This sharing configuration maps the **/data/xnat/home** folder to **../../../home** (in other words, a home folder located right outside of the **xnat-vagrant** source repository). It then maps the **/data/xnat/home/.ssh** folder to the **.ssh** folder inside the already shared **/data/xnat/home** folder. This is necessary because of the file and directory permissions set at each level:

- The VM user's home folder and its subfolders must have no greater access level than full user access with group and world read and execute access ([this post](http://blog.superuser.com/2011/04/22/linux-permissions-demystified/) has a good explanation of how the mode numbers above set the file and directory permissions). Restricting _all_ group and world access can make for a difficult working environment, though.
- The VM user's **.ssh** folder is a special case. Public/private key authentication will **not** be allowed if the permissions on the **.ssh** folder or the **authorized__key** file within that folder allow any group or world access.

You also must make sure that you include the correct version of **authorized__key** or you won't be able to access the VM through ssh.

#### Configuring SMTP

The XNAT Vagrant project includes a script named `configure-postfix`. This script simplifies configuring SMTP for your XNAT application by installing Postfix on the VM and configuring it as an SMTP relay for an actual SMTP server. This is usually easier and more reliable than setting up a complex configuration in XNAT itself, especially when the SMTP service requires encryption and authentication.

To configure Postfix on your VM, you must use either `sudo` or run the script as the root user. The script accepts the following parameters:

Short | Long | Description
----- | ---- | -----------
-h | --help | Display this help message
-s | --server | The SMTP server address
-p | --port | The SMTP server port (defaults to 587 if username is specified, 25 if not)
-H | --host | The mail host (defaults to xnatdev)
-u | --username | The username for accessing the SMTP server
-P | --password | The password for accessing the SMTP server
-t | --test-to | Email address to specify as to address on test email
-f | --test-from | Email address to specify as from address on test email

The only parameter that is required is the `-s`/`--server` parameter, which specifies the address of the SMTP server that actually sends the emails. If you have an SMTP server that doesn't require authentication or encryption (typically found in institutions and companies within a firewall or local network environment), then that's all you need. For other services, such as SMTP servers with popular email providers like Google or Yahoo, you'll also need at least a username and password.

The `--test-to` and `--test-from` parameters are very useful for validating that the configured SMTP relay works properly. If the script finds values for those, it sends an email from the value for the `--test-from` parameter to the address specified in the value for the `--test-to` parameter. In the example below, that means that `user2@example.edu` should receive an email from `user1@example.edu`:

```bash
configure-postfix --server smtp.gmail.com --port 587 --username "abc123@gmail.com" --password <app-password> --host example.edu --test-from "user1@example.edu" --test-to "user2@example.edu"
```
